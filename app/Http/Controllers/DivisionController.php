<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Division;

class DivisionController extends Controller
{
    public function index(){
        $data = Division::where("id", 2)->first()->members;

        return response()->json([
            "data" => $data,
        ], 200);
    }

    public function store(Request $request){
        $fields = $request->validate([
            "name" => "required"
        ]);

        $data = [
            "name" => $fields["name"],
        ];

        $data = Division::create($data);

        return response()->json([
            "message" => "Success",
            "data" => $data,
        ], 201);
    }
}
