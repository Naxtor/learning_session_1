<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class AuthController extends Controller
{
    public function register(Request $request){
        $fields = $request->validate([
            "name" => "required",
            "email" => "required|email:rfc,dns",
            "password" => "required|min:10"
        ]);
        
        $user = User::firstOrCreate([
            "email" => $fields["email"]
        ], [
            "name" => $fields["name"],
            "password" => \Hash::make($fields["password"]),
        ]);

        return response()->json([
            "message" => "Success",
            "data" => $user,
            
        ], 201);
    }

    public function login(Request $request){
        $fields = $request->validate([
            "email" => "required|email:rfc,dns",
            "password" => "required|min:10"
        ]);

        $user = User::where("email", $fields["email"])->first();

        if(!$user || !\Hash::check($fields["password"], $user->password)){
            return response()->json([
                "message" => "Not found",
            ], 404);
        }

        // Deleting old tokens
        $user->tokens()->delete();
        $token = $user->createToken("auth_token")->plainTextToken;

        return response()->json([
            "message" => "Success",
            "data" => $user,
            "token" => $token,
        ], 200);
    }
}
