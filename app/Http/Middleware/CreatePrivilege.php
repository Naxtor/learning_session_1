<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CreatePrivilege
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->user()->email_verified_at == null){
            return response()->json([
                "message" => "Unauthorized",
            ], 401);
        }

        return $next($request);
    }
}
